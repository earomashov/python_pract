"""
1) Получить список файлов в папке
2) Получить текущую директорию
3) Создать папку
4) Склеить путь из папки и файла
5) Посчитать сколько в каталоге установки python:
•папок
•файлов с расширением ".py"
•файлов с расширением ".exe"
•всего файлов
• Записать результаты в файл
"""


import os
import sys

cwd_path = os.getcwd()
python_path = sys.base_exec_prefix
result_file_name = 'result.txt'
result_dir = 'result'
result_path = os.path.join(result_dir, result_file_name)


def get_file_list(path):
    """
    Возвращает список файлов в директории path (включая вложенные директории)
    :param path:
    :return:
    """
    lst = []
    for root, dirs, files in os.walk(path):
        lst.extend(files)
    return lst


def get_entry_count(path, ext_list: list):
    """
    Считает количество файлов, папок и файлов с расширениями из ext_list в папке path
    :param path: папка, в которой считать
    :param ext_list: список расширений, которые считать отдельно
    :return: словарь вида {'ext1': int, 'ext2': int, ..., 'dir': int, 'file': int}
    """
    res = dict.fromkeys(ext_list, 0)
    for root, dirs, files in os.walk(path):
        res['dir'] = res.get('dir', 0) + len(dirs)
        res['file'] = res.get('file', 0) + len(files)

        for file in files:
            for ext in ext_list:
                # если расширение есть в списке считаемых
                if os.path.splitext(file)[1] == ext:
                    res[ext] += 1
    return res


def dict_to_file(data: dict, file_name: str):
    """
    Запись словаря в текстовый файл
    :param data: словарь
    :param file_name: файл, создается если отсутствует
    :return:
    """
    with open(file_name, 'w') as out:
        for key, val in data.items():
            out.write(f'{key}: {val}\n')


print(f'Текущая директория: {cwd_path}')
print(f'Список файлов в директории {cwd_path}:')
print(get_file_list(cwd_path))

# создать папку result_dir если отсутствует
if not os.path.exists(result_dir):
    os.mkdir(result_dir)

entry_count = get_entry_count(python_path, ['.py', '.exe'])
print(f'Количество элементов в директории {python_path}: {entry_count}')
dict_to_file(entry_count, result_path)
