"""
Проверить, что номера телефонов состоят только из цифр.
Они могут начинаться с «+», цифры могут быть разделены пробелами и дефисами «-»
Например: 8-999-777-1111, +7 999 333 2222, +7 999-555-11-11
"""
import re


def check_phone_number(number: str):
    s = number.replace(' ', '').replace('-', '').replace('+', '')
    return s.isdigit() and len(s) == 11


def check_phone_number2(number: str):
    phone_regex = re.compile(r'^(\+?\d{1,2}[-\s]?)?[0-9]{3}[-\s]?[0-9]{3,4}[-\s]?[0-9]{2}[-\s]?[0-9]{2}$')
    return phone_regex.match(number)


data = ['+7-999-777-1111',
        '+7 999 333 2222',
        '+7 999 555-11-11',
        '8 999 555-11-11',
        '89995551111',
        '+7999555-11-11',
        '+79955511111',
        '8920 6665544',
        '+7 9998882211',
        '+8999 45666-11',
        '8999888-22-11',
        '+7 920-655 5420']

for phone in data:
    assert check_phone_number(phone)
for phone in data:
    assert check_phone_number2(phone)
