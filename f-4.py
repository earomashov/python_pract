"""
Поменять местами самый большой исамый маленький элементы списка
"""


lst = [-2, 40, 6, 0, -9, 4, -1]
print(lst)

if lst:
    min_index = lst.index(min(lst))
    max_index = lst.index(max(lst))
    lst[min_index], lst[max_index] = lst[max_index], lst[min_index]

print(lst)
