"""
Напишите декоратор func_time, который подсчитывает и выводит сколько времени выполняется функция,
обернутая в него
"""


import time


def func_time(func):
    """
    Замеряет время работы функции func
    :param func:
    :return:
    """
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f'Функция {func.__name__} работала {end - start} сек.')
        return result
    return wrapper


@func_time
def some_func(delay):
    time.sleep(delay)
    

@func_time
def some_func2(delay):
    time.sleep(delay)
    

some_func(1)
some_func2(3)
