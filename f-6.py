"""
Напишите функцию, которая подсчитывает количество счастливых шестизначных билетов.
Билеты начинаются с 000000 и заканчиваются 999999.
Счастливым считается билет, если сумма первых трех значений, равна сумме вторых трёх
(Например: 927864, 123006, 002110 и т.д.)
"""


import time


# декоратор из задачи 7
def func_time(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f'Функция {func.__name__} работала {end - start} сек.')
        return result
    return wrapper


@func_time
def get_lucky():
    n = 0
    for i in range(1000000):
        if sum(map(int, str(i // 1000))) == sum(map(int, str(i % 1000))):
            n += 1
    return n


# 2й способ, на 25-30% быстрее
@func_time
def get_lucky2():
    n = 0
    for i in range(1000000):
        x = i // 1000
        y = i % 1000
        s1 = s2 = 0
        for j in range(3):
            s1 += x % 10
            x = x // 10
            s2 += y % 10
            y = y // 10
        if s1 == s2:
            n += 1
    return n
    
    
print(get_lucky())
print(get_lucky2())
