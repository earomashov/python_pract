from datetime import datetime
from datetime import timedelta
import calendar

"""
Получить текущую дату +5 дней в формате ДД.ММ.ГГ
"""
d = (datetime.now() + timedelta(days=5)).strftime('%d.%m.%y')
print(f'Текущая дата + 5 дней: {d}')

"""
Получить текущую дату в формате ДД.ММ.ГГГГ
"""
d = datetime.now().strftime('%d.%m.%Y')
print(f'Текущая дата: {d}')

"""
На входе есть строка '03.05.13' надо к этой дате прибавить 10 дней и вывести в формате ДД.ММ.ГГГГ
"""
d = '03.05.13'
add_days = 10
d1 = datetime.strptime(d, '%d.%m.%y')
d1 = (d1 + timedelta(days=add_days)).strftime('%d.%m.%Y')
print(f'{d} + {add_days} дней: {d1}')

"""
Вывести дату в формате ДД.ММ.ГГ, эта дата должна быть первым днем месяца
"""
d = datetime.now()
d1 = d.replace(day=1).strftime('%d.%m.%y')
print(f"Первый день месяца (дата {d.strftime('%d.%m.%y')}): {d1}")

"""
Вывести дату в формате ДД.ММ.ГГ, эта дата должна быть последним днем месяца
"""
d = '01.02.24'
d = datetime.strptime(d, '%d.%m.%y')
d1 = (d.replace(day=1, month=d.month % 12 + 1) - timedelta(days=1)).replace(year=d.year).strftime('%d.%m.%y')
print(f"Последний день месяца (дата {d.strftime('%d.%m.%y')}): {d1}")

"""
Прибавить к любой дате 1 месяц и вывести в формате ДД.ММ.ГГГГ
"""
d = '31.05.13'
d = datetime.strptime(d, '%d.%m.%y')
d1 = (d.replace(day=min(d.day, calendar.monthrange(d.year, d.month % 12 + 1)[1]),
                month=d.month % 12 + 1,
                year=d.year if d.month < 12 else d.year + 1)).strftime('%d.%m.%Y')
print(f'{d.strftime("%d.%m.%Y")} + 1 месяц: {d1}')


def change_month(date: datetime, month_count):
    """
    К дате date прибавляет/вычитает month_count месяцев
    """
    month = date.month - 1 + month_count
    year = date.year + month // 12
    month = month % 12 + 1
    day = min(date.day, calendar.monthrange(year, month)[1])
    return datetime(year, month, day)


d = '31.12.23'
add_month = 14
d = datetime.strptime(d, '%d.%m.%y')
print(f"{d.strftime('%d.%m.%Y')} + {add_month} месяцев: {change_month(d, add_month).strftime('%d.%m.%Y')}")
