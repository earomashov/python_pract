"""
Дана строка, содержащая полное имя файла
(например, C:\development\inside\test-project_management\inside\myfile.txt').
Выделите из этой строки имя файла без расширения
"""


file_name = "C:\\development\\inside\\test-project_management\\inside\\myfile.txt"

file_name = file_name[file_name.rfind("\\") + 1:]
dot_pos = file_name.rfind(".")
file_name = file_name[:len(file_name) if dot_pos == -1 else dot_pos]

print(file_name)
